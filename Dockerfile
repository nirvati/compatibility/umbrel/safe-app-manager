FROM debian:bookworm-slim as builder

WORKDIR /policy

RUN apt update && apt install -y curl

RUN curl -L -o /usr/local/bin/opa https://openpolicyagent.org/downloads/v0.64.1/opa_linux_$(dpkg --print-architecture)_static && chmod +x /usr/local/bin/opa

COPY nirvati.rego /policy/nirvati.rego

RUN opa build --bundle --output nirvati.tar.gz .

FROM lipanski/docker-static-website:latest

COPY --from=builder /policy/nirvati.tar.gz .
