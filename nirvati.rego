package docker.nirvati

import rego.v1

default allow := false

allow if not deny

deny if privileged

deny if host_devs

deny if cgroup_parent

deny if cgroup

deny if unsafe_caps

deny if unsafe_pid

deny if unsafe_network

deny if security_opt

privileged if input.Body.HostConfig.Privileged == true

host_devs if count(input.Body.HostConfig.Devices) > 0

cgroup_parent if input.Body.HostConfig.CgroupParent != ""

cgroup if input.Body.HostConfig.Cgroup != ""

unsafe_caps if count(input.Body.HostConfig.CapAdd) > 0

unsafe_pid if input.Body.HostConfig.PidMode != ""

unsafe_network if input.Body.HostConfig.NetworkMode == "host"

security_opt if count(input.Body.HostConfig.SecurityOpt) > 0
